package info.hccis.performancehall_mobileappbasicactivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;
/**
 *Database class and method to add a new ticket order into the database
 *
 * @author a_leo
 * @since 20220301
 */
public class DBaseTicketOrder {
    private DatabaseReference databaseReference;

    public DBaseTicketOrder() {
        FirebaseDatabase db = FirebaseDatabase.getInstance();
        databaseReference = db.getReference(TicketOrder.class.getSimpleName());
    }

    public void addTicketDatabase(TicketOrder ticketOrder) {
        databaseReference.push().setValue(ticketOrder);
    }
}
